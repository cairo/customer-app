package usertests;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="features/userfeatures",
        snippets= SnippetType.CAMELCASE,
		glue = { "classpath:usertests" } )

/**
 * @author Mathias Kirkeskov Madsen
 */
public class UserTest {

}
