package usertests;
import javax.ws.rs.core.Response;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objects.User;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

/**
 * @author Mathias Kirkeskov Madsen
 * For performing cucumber tests from the customer side
 */
public class UserSteps {

    User customer = new User();
    Client client = ClientBuilder.newClient();
    WebTarget webtarget = client.target("http://02267-cairo.compute.dtu.dk:8383/usermanager");
    Response createUserResponse;
    Response deleteUserResponse;
    ArrayList<String> testUserCprs = new ArrayList<String>();

    @After
    public void databaseCleanup(){
        System.out.println("The database was wiped");
        for(String cpr: testUserCprs){
            System.out.println("User with cpr: " + cpr + " was wiped");
            deleteUser(cpr);
        }
    }

    @Given("^a customer with cpr \"([^\"]*)\" and first name: \"([^\"]*)\" and last name: \"([^\"]*)\"$")
    public void aCustomerWithCprAndFirstNameAndLastName(String arg1, String arg2, String arg3){
    	customer.setCpr(arg1);
        customer.setFirstName(arg2);
        customer.setLastName(arg3);
    }

    @And("^last name: \"([^\"]*)\" and account number \"([^\"]*)\"$")
    public void lastNameAndAccountNumber(String arg0, String arg1){
        customer.setLastName(arg0);
        customer.setAccount(arg1);
    }

    @And("^the customer does not already exist in the DTUpay system$")
    public void theCustomerDoesNotAlreadyExistInTheDTUpaySystem() throws Throwable {
        boolean isCustomer = true;
        String returned = validate(customer.getCpr(), isCustomer);
        assertEquals("User Not Found", returned);
    }

    @When("^the customer tries to register to DTUpay$")
    public void theCustomerTriesToRegisterToDTUpay() throws Throwable {
        boolean isCustomer = true;
        createUserResponse = createUser(customer, isCustomer);

    }

    @Then("^the customer will be succesfully registered$")
    public void theCustomerWillBeSuccesfullyRegistered() throws Throwable {
        boolean isCustomer = true;
        assertEquals("The user was added", createUserResponse.readEntity(String.class));
        assertEquals(200, createUserResponse.getStatus());
        String returned = validate(customer.getCpr(), isCustomer);
        assertEquals("User Found", returned);
    }

    @And("^the customer exists in the DTUpay system$")
    public void theCustomerExistsInTheDTUpaySystem() throws Throwable {
        boolean isCustomer = true;
        Response status = createUser(customer, isCustomer);
        assertEquals(200, status.getStatus());
    }

    @Then("^the customer will not be registered again$")
    public void theCustomerWillNotBeRegistered() throws Throwable {
        assertEquals("The user already exists", createUserResponse.getEntityTag().getValue());
        assertEquals(304, createUserResponse.getStatus());
        boolean isCustomer = true;
        String returned = validate(customer.getCpr(), isCustomer);
        assertEquals("User Found", returned);
    }

    @When("^the customer requests to be deleted$")
    public void theCustomerRequestsToBeDeleted() throws Throwable {
        deleteUserResponse = deleteUser(customer.getCpr());

    }

    @Then("^the customer does not exist in the database anymore$")
    public void theCustomerDoesNotExistInTheDatabaseAnymore() throws Throwable {
        boolean isCustomer = true;
        assertEquals(200, deleteUserResponse.getStatus());
        assertEquals("The user was deleted", deleteUserResponse.readEntity(String.class));
        String returned = validate(customer.getCpr(), isCustomer);
        assertEquals("User Not Found", returned);
    }

    @Then("^the customer will be told that they are not in the system already$")
    public void theCustomerWillBeToldThatTheyAreNotInTheSystemAlready() throws Throwable {
        boolean isCustomer = true;
        assertEquals(304, deleteUserResponse.getStatus());
        assertEquals("The user was not found", deleteUserResponse.getEntityTag().getValue());
        String returned = validate(customer.getCpr(), isCustomer);
        assertEquals("User Not Found", returned);
    }

    private String validate(String cpr, boolean isCustomer){
        WebTarget webTargetFinal = webtarget.path("/" + cpr).queryParam("iscustomer", isCustomer);
        Invocation.Builder invocationBuilder = webTargetFinal.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.get();
        return response.readEntity(String.class);
    }

    private Response createUser(User user, boolean isCustomer){
        WebTarget webTargetUser = webtarget.queryParam("iscustomer", isCustomer);
        Invocation.Builder invocationBuilder = webTargetUser.request(MediaType.TEXT_PLAIN);
        if(!testUserCprs.contains(user.getCpr())){
            testUserCprs.add(user.getCpr());
        }
        return invocationBuilder.post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    private Response deleteUser(String cpr){
        WebTarget webTargetUser = webtarget.path("/" + cpr);
        Invocation.Builder invocationBuilder = webTargetUser.request(MediaType.TEXT_PLAIN);
        return invocationBuilder.delete();
    }
}
