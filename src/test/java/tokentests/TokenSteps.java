package tokentests;

import static org.junit.Assert.assertEquals;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objects.RestUser;
import objects.User;

/**
 * @author Alexander
 * Class implementing the steps for the token features.
 * Tests user scenarios for requesting tokens
 */

//This is a nice class
public class TokenSteps {
	
	User customer = new User();
	Response getTokenResponse;
	String requestedTokens;    

	/**
	 * @author Alexander
	 * Generates a customer with provided values from the scenario
	 * @param String arg1 Provided CPR number
	 * @param String arg2 Provided first name
	 * @param String arg3 Provided last name
	 * @return nothing
	 */
	@Given("^a customer with cpr \"([^\"]*)\" and first name: \"([^\"]*)\" and last name: \"([^\"]*)\"$")
	public void aCustomerWithCprAndFirstNameAndLastName(String arg1, String arg2, String arg3) {
        customer.setCpr(arg1);
        customer.setFirstName(arg2);
        customer.setLastName(arg3);
    }

	/**
	 * @author Alexander
	 * Requests tokens using the generated customer
	 * @param int arg1 Provided amount of tokens requested
	 * @return nothing
	 */
    @When("^the customer requests (\\d+) tokens$")
    public void theCustomerRequestsTokens(int arg1){
		getTokenResponse = requestTokens(Integer.toString(arg1));
		requestedTokens = getTokenResponse.readEntity(String.class);
    }

	/**
	 * @author Alexander
	 * Tests the returned token request contains the specified amount of tokens
	 * @param int arg1 Provided amount of tokens requested
	 * @return nothing
	 */
    @Then("^the customer retrieves the (\\d+) tokens$")
    public void theCustomerRetrievesTheTokens(int arg1){
    	assertEquals(200, getTokenResponse.getStatus());
    	JSONArray resp = new JSONArray(requestedTokens);
    	assertEquals(arg1,resp.length());
    }
    
	/**
	 * @author Alexander
	 * Tests if a user contains too many valid tokens for requesting
	 * @param int arg1 Provided amount of tokens requested
	 * @return nothing
	 */
    @Then("^has (\\d+) tokens in the DTUPay system$")
    public void hasTokensInTheDTUPaySystem(int arg1) {
    	// Checks that new tokens cannot be requested, as we cannot check valid tokens for a user directly
        getTokenResponse = requestTokens("3");
        assertEquals(304, getTokenResponse.getStatus());
    	assertEquals("user has too many tokens (more than 1)",getTokenResponse.getEntityTag().getValue());
    }
    
	/**
	 * @author Alexander
	 * Ensures a customer has more than 1 token
	 * @param int arg1 Provided amount of tokens the user should have more than
	 * @return nothing
	 */
    @Given("^the customer has more than (\\d+) token$")
    public void theCustomerHasMoreThanToken(int arg1){
        getTokenResponse = requestTokens("3");
        assertEquals(200, getTokenResponse.getStatus());
    }
    
	/**
	 * @author Alexander
	 * Tests that a user has too many tokens to request
	 * @return nothing
	 */
    @Then("^the customer is informed of having too many valid tokens$")
    public void theCustomerIsInformedOfHavingTooManyValidTokens() {
    	assertEquals(304, getTokenResponse.getStatus());
    	assertEquals("user has too many tokens (more than 1)",getTokenResponse.getEntityTag().getValue());
    }
    
	/**
	 * @author Alexander
	 * Tests that a customer is told to request between 1 and 5 tokens if amount outside this
	 * @return nothing
	 */
    @Then("^the customer is informed of requesting too many tokens$")
    public void theCustomerIsInformedOfRequestingTooManyTokens() {
    	assertEquals(304, getTokenResponse.getStatus());
    	assertEquals("token amount not between 1 and 5",getTokenResponse.getEntityTag().getValue());
    }
    
    /**
     * @author Alexander
     * Function to request tokens via a Token Restadapter. Used in multiple tests.
     * @param String amount 
     * @return
     */
    private Response requestTokens(String amount) {
		RestUser user = new RestUser();
		user.username= customer.getFirstName()+customer.getLastName();
		user.userID = customer.getCpr();

		Client client = ClientBuilder.newClient();
		WebTarget webtarget = client.target("http://02267-cairo.compute.dtu.dk:8080/tokenmanager");
		WebTarget webtargetToken = webtarget.queryParam("amount",amount);
		Invocation.Builder invocationBuilder = webtargetToken.request(MediaType.APPLICATION_JSON);
		return invocationBuilder.post(Entity.entity(user,MediaType.APPLICATION_JSON));
    }
}
