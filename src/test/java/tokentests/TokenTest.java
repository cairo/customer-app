package tokentests;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
/**
 * @author Alexander
 * Test class setup for cucumber token tests
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="features/tokenfeatures",
        snippets= SnippetType.CAMELCASE,
		glue = { "classpath:tokentests" } )
        
public class TokenTest {

}
