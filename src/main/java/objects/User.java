package objects;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Henning
 *
 * This is a duplicate of the user class which already exists in the user service.
 * This class has been created, such that we can send user objects to the rest adapter.
 */
@XmlRootElement
public class User {

    public User() {

    }

    private String cpr;

    private String firstName;

    private String lastName;

    private String account;

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return user.cpr.equals(this.cpr) &&
                user.firstName.equals(this.firstName) &&
                user.lastName.equals(this.lastName) &&
                user.account.equals(this.account);
    }

}