package objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Alexander
 *
 * This class has the same properties as the user class in the token microservice.
 * Object to add in communication through the Rest adapter to the Token service.
 */
@XmlRootElement
public class RestUser {
	public String userID;
	public String username;

	public RestUser(){
	}
}
