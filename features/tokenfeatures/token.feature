Feature: User requests and gets tokens

Scenario: Tokens are successfully requested.
          Given a customer with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
          When the customer requests 3 tokens
          Then the customer retrieves the 3 tokens
          And has 3 tokens in the DTUPay system
          
Scenario: The customer has too many valid tokens
          Given a customer with cpr "0000000002" and first name: "Henning" and last name: "Frederiksen"
          And the customer has more than 1 token
          When the customer requests 2 tokens
          Then the customer is informed of having too many valid tokens
          
Scenario: The customer requests too many tokens
          Given a customer with cpr "0000000002" and first name: "Henning" and last name: "Frederiksen"
          When the customer requests 6 tokens
          Then the customer is informed of requesting too many tokens