Feature: User creation

Scenario: A customer can be successfully created
          Given a customer with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
          And the customer does not already exist in the DTUpay system
          When the customer tries to register to DTUpay
          Then the customer will be succesfully registered
          
Scenario: A customer can not be created twice
          Given a customer with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
          And the customer exists in the DTUpay system
          When the customer tries to register to DTUpay
          Then the customer will not be registered again
          
Scenario: A customer can be successfully deleted
		  Given a customer with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
    	  And the customer exists in the DTUpay system
          When the customer requests to be deleted
          Then the customer does not exist in the database anymore

Scenario: A customer can not be deleted if they do not exist in the system
          Given a customer with cpr "0000000001" and first name: "Henning" and last name: "Frederiksen"
          And the customer does not already exist in the DTUpay system
          When the customer requests to be deleted
          Then the customer will be told that they are not in the system already
